### 2022-12-07
Changes since 2022-07-06:
- Added LSC font customization for A13 QPR1
- Updated Last Resort font (Unicode 15)
- Improved Gapps font changing support (integrated @MrCarb0n's KillGMSFont)
- Config: removed GS customization (Pixel)
- ...

[Full Changelog](https://gitlab.com/nongthaihoang/oh_my_font/-/commits/master)
